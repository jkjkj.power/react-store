import { useEffect, useState } from "react";
import { getLatestRUBtoUSD } from "../api/getLatestRUBtoUSD";
import { mockApi } from "../mocks";
import { Item } from "../mocks/mockApi";

export const useStoreItems = () => {
  const [items, setItems] = useState<Item[]>([]);

  useEffect(() => {
    (async () => {
      const usd = await getLatestRUBtoUSD();
      const mockData = await mockApi();

      const data = mockData.map((item) => ({
        ...item,
        priceRUB: item.priceUSD * usd,
      }));

      setItems(data);
    })();
  }, []);

  const updateUSD = async () => {
    setItems([]);

    const usd = await getLatestRUBtoUSD();
    const data = items.map((item) => ({
      ...item,
      priceRUB: item.priceUSD * usd,
    }));

    setItems(data);
  };

  return { items, setItems, updateUSD };
};
