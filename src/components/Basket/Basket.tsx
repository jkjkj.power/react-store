import { CSSProperties, FC } from "react";
import { Item } from "../../mocks/mockApi";
import { ItemCard } from "../ItemCard";
import { Button } from "../Button";

import "./Basket.styles.css";

type Props = {
  items: Item[];
  handleBuy: () => void;
};

export const Basket: FC<Props> = ({ items, handleBuy }) => {
  return (
    <div className="basket">
      <div className="list">
        {items.map((item) => (
          <ItemCard isOpenBasket key={item.id} {...item} />
        ))}
      </div>
      <Button style={buttonStyles} onClick={handleBuy}>
        Купить
      </Button>
    </div>
  );
};

const buttonStyles: CSSProperties = {
  backgroundColor: "#96a9d6",
  color: "#000000",
  width: "50%",
};
