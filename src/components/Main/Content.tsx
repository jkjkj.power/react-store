import { FC } from "react";
import { Item } from "../../mocks/mockApi";
import { Basket } from "../Basket";
import { ItemCard } from "../ItemCard";

import "./Main.styles.css";

type Props = {
  isOpenBasket: boolean;
  items: Item[];
  basketItems: Item[];
  handleAddItems: (item: Item) => void;
  handleBuy: () => void;
};

export const Content: FC<Props> = ({
  isOpenBasket,
  items,
  basketItems,
  handleAddItems,
  handleBuy,
}) => {
  if (isOpenBasket) {
    return <Basket items={basketItems} handleBuy={handleBuy} />;
  }

  return (
    <div className="content">
      {!items.length ? (
        <div className="loading">Загрузка товаров/обновление цен...</div>
      ) : (
        <>
          {items.map((item) => (
            <ItemCard
              key={item.id}
              {...item}
              isOpenBasket={isOpenBasket}
              addToBasket={handleAddItems}
            />
          ))}
        </>
      )}
    </div>
  );
};
