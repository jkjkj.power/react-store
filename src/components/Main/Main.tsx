import { useState } from "react";
import { TopPanel } from "../TopPanel";
import { Item } from "../../mocks/mockApi";
import { contItemsInBasket } from "../../utils/contItemsInBasket";
import { useStoreItems } from "../../hooks/useStoreItems";
import { getItemsWithCount, updateStockAFterBuying } from "../../utils";
import { Content } from "./Content";

import "./Main.styles.css";

export const Main = () => {
  const [isOpenBasket, setIsOpenBasket] = useState(false);
  const [count, setCount] = useState(0);
  const [wasBuying, setWasBuying] = useState(false);

  // TODO: переделать в один массив
  const [basketItems, setBasketItems] = useState<Item[]>([]);
  const { items, setItems, updateUSD } = useStoreItems();
  //

  const handleAddItems = (item: Item) => {
    setCount((prev) => (prev += 1));

    setBasketItems((prev) => [...prev, item]);
  };

  const openBasket = () => {
    if (!basketItems.length) {
      return;
    }
    setWasBuying(false);

    const mapBasketItemsCount = contItemsInBasket(basketItems);
    const itemsWithCount = getItemsWithCount(items, mapBasketItemsCount);

    setBasketItems(itemsWithCount);
    setIsOpenBasket(true);
  };

  const closeBasket = () => {
    if (wasBuying) {
      const updatedItems = updateStockAFterBuying(items, basketItems);
      setCount(0);
      setBasketItems([]);
      setItems(updatedItems);
    }

    setIsOpenBasket(false);
  };

  const handleBuy = () => {
    if (wasBuying) {
      return;
    }

    const withUpdatedStock = basketItems.map((item: Item) => ({
      ...item,
      stock: item.stock - item.count,
    }));

    setBasketItems(withUpdatedStock);
    setWasBuying(true);
  };

  return (
    <>
      <div>
        <TopPanel
          itemsCount={count}
          isOpenBasket={isOpenBasket}
          updateUSD={updateUSD}
          openBasket={openBasket}
          closeBasket={closeBasket}
        />

        <Content
          handleBuy={handleBuy}
          items={items}
          basketItems={basketItems}
          isOpenBasket={isOpenBasket}
          handleAddItems={handleAddItems}
        />
      </div>
    </>
  );
};
