import { CSSProperties, FC } from "react";

import "./Button.styles.css";

type Props = {
  onClick: () => void;
  style?: CSSProperties;
  disabled?: boolean;
};

export const Button: FC<Props> = ({ children, style, onClick, disabled }) => {
  return (
    <button
      disabled={disabled}
      onClick={onClick}
      style={{ ...style }}
      className="button"
    >
      {children}
    </button>
  );
};
