import { BasketIcon, Logo } from "../../icons";
import { Button } from "../Button";
import { CSSProperties, FC } from "react";

import "./TopPanel.styles.css";

type Props = {
  isOpenBasket: boolean;
  itemsCount: number;

  updateUSD: () => void;
  openBasket: () => void;
  closeBasket: () => void;
};

export const TopPanel: FC<Props> = ({
  isOpenBasket,
  itemsCount,
  openBasket,
  closeBasket,
  updateUSD,
}) => {
  return (
    <div className="top-panel">
      <Logo />

      {isOpenBasket ? (
        <Button style={backButtonStyle} onClick={closeBasket}>
          Вернуться
        </Button>
      ) : (
        <div className="buttons">
          <Button style={updateCurrencyButtonStyles} onClick={updateUSD}>
            Обновить цены
          </Button>
          <Button style={basketButtonStyle} onClick={openBasket}>
            <BasketIcon /> {itemsCount}
          </Button>
        </div>
      )}
    </div>
  );
};

//TODO: подумать, куда вынести константы, либо найти другой подход
const updateCurrencyButtonStyles: CSSProperties = {
  width: "150px",
  backgroundColor: "#FFFFFF",
  color: "#000000",
};

const basketButtonStyle: CSSProperties = {
  display: "flex",
  justifyContent: "space-around",
  alignItems: "center",
  color: "#000000",
};

const backButtonStyle: CSSProperties = {
  display: "flex",
  justifyContent: "space-around",
  alignItems: "center",
  color: "#000000",
  width: "150px",
};
