import { CSSProperties, FC } from "react";
import { Button } from "../Button";
import { Item } from "../../mocks/mockApi";
import CoC from "../../assets/captain_of_crush.png";

import "./ItemCard.styles.css";

type Props = {
  isOpenBasket?: boolean;
  addToBasket?: (item: Item) => void;
} & Item;

export const ItemCard: FC<Props> = (item) => {
  const { name, priceRUB, priceUSD, stock, addToBasket, isOpenBasket } = item;

  return (
    <div className="card">
      <img className="img" src={CoC} alt="" />
      <div className="info">
        <div className="name">
          <b>{name}</b>
        </div>
        <div className="price">
          RUB: <b>{priceRUB}</b>{" "}
        </div>
        <div className="price">
          USD: <b>{priceUSD}</b>{" "}
        </div>
        <div className="stock">
          Остаток на складе: <b>{stock}</b>{" "}
        </div>
        <br />
        {isOpenBasket && (
          <div className="stock">
            В корзине: <b>{item.count}</b>{" "}
          </div>
        )}
      </div>
      {!isOpenBasket && (
        <Button style={buttonStyles} onClick={() => addToBasket(item)}>
          В корзину
        </Button>
      )}
    </div>
  );
};

const buttonStyles: CSSProperties = {
  backgroundColor: "#96a9d6",
  color: "#000000",
  width: "100%",
};
