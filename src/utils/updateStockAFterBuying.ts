import { Item } from "../mocks/mockApi";

//TODO: убрать/поправить, как уберется два одинаковых массива продуктов
export const updateStockAFterBuying = (items: Item[], basketItems: Item[]) => {
  return items.map((item) => {
    basketItems.forEach((bItem) => {
      if (bItem.id === item.id) {
        item.stock = bItem.stock;
      }
    });

    return item;
  });
};
