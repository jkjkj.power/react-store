import { Item } from "../mocks/mockApi";

export type ItemCountsType = {
  [key: Item["id"]]: number;
};

export const contItemsInBasket = (items: Item[]) => {
  return items.reduce((acc, item) => {
    const key = item.id;

    if (acc[key]) {
      acc[key] += 1;
    } else {
      acc[key] = 1;
    }

    return acc;
  }, {} as ItemCountsType);
};
