import { Item } from "../mocks/mockApi";
import { ItemCountsType } from "./contItemsInBasket";

export const getItemsWithCount = (
  items: Item[],
  mapBasketItemsCount: ItemCountsType,
) => {
  const itemsWithCount = [] as Item[];

  items.forEach((item) => {
    return Object.entries(mapBasketItemsCount).forEach(([id, count]) => {
      if (id === item.id) {
        itemsWithCount.push({ ...item, count });
      }
    });
  });

  return itemsWithCount;
};
