import { items } from "./items";

export type Item = {
  id: string;
  name: string;
  priceRUB: number;
  priceUSD: number;
  stock: number;
  count: number
};

export const mockApi = () => {
  return new Promise<Item[]>((res, rej) => {
    setTimeout(() => {
      res(items);
    }, 900);
  });
};
