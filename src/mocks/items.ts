export const items = [
  {
    id: "113ho1i2n3oI",
    name: "Captain of Crush #1.5",
    priceRUB: 4000,
    priceUSD: 40,
    stock: 56,
    count: 0,
  },
  {
    id: "113ho1i2n3oZ",
    name: "Captain of Crush #2",
    priceRUB: 4000,
    priceUSD: 40,
    stock: 44,
    count: 0,
  },
  {
    id: "113ho1i2n3oo",
    name: "Captain of Crush #2.5",
    priceRUB: 4000,
    priceUSD: 40,
    stock: 53,
    count: 0,
  },
];
