//TODO: вынести в env
const API_KEY = "de565e45955eaf6c3e230240";

export const getLatestRUBtoUSD = async () => {  
  const response = await fetch(
    `https://v6.exchangerate-api.com/v6/${API_KEY}/latest/USD`,
  );

  const data = await response.json();

  return data.conversion_rates.RUB;
};
